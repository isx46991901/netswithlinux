	#### 1. ip a

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"
	
	ip r f all
	ip a f dev enp2s0
	ip a
	[...]
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 14:da:e9:b6:e8:ea brd ff:ff:ff:ff:ff:ff

	ip r
	
Ponte las siguientes ips en tu tarjeta ethernet: 
2.2.2.2/24, 
3.3.3.3/16, 
4.4.4.4/25

	# Ordenes
	ip a a 2.2.2.2/24 dev enp2s0
	ip a a 3.3.3.3/16 dev enp2s0
	ip a a 4.4.4.4/25 dev enp2s0
	ip a s dev enp2s0
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 14:da:e9:b6:e8:ea brd ff:ff:ff:ff:ff:ff
    inet 2.2.2.2/24 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 3.3.3.3/16 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 4.4.4.4/25 scope global enp2s0
       valid_lft forever preferred_lft forever

Consulta la tabla de rutas de tu equipo
	
	ip route show
	2.2.2.0/24 dev enp2s0  proto kernel  scope link  src 2.2.2.2 
	3.3.0.0/16 dev enp2s0  proto kernel  scope link  src 3.3.3.3 
	4.4.4.0/25 dev enp2s0  proto kernel  scope link  src 4.4.4.4 

	

Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132

Las que sale el mensaje "Network is unreacheable" és porque no estamos en ninguna red.
La que devuelve el ping és porque hacemos ping a nuestra própia máquina.

#### 2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	ip r f all

Conecta una segunda interfaz de red por el puerto usb

	ip link show
	[...]
	3: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff


Cambiale el nombre a usb0

	ip link set usb0 down
	ip link set usb0 name usb3
	ip link show
	[...]
	3: usb3: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff

Modifica la dirección MAC

	ip link set usb3 address 00:11:22:33:44:55			
	ip link show
	[...]
	3: usb3: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000
    link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff

	
Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.

	ip a a 5.5.5.5/24 dev usb3
	ip a
	[...]
	3: usb3: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff
    inet 5.5.5.5/24 scope global usb3
       valid_lft forever preferred_lft forever

Observa la tabla de rutas

	ip a f dev usb3
	ip a a 5.5.5.5/24 dev usb3
	ip link set usb3 up
	ip route show
	5.5.5.0/24 dev usb3  proto kernel  scope link  src 5.5.5.5 linkdown 

#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet
	
	# rutas
	ip r f all
	# ips
	ip a f dev enp2s0
	
En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

	ip a a 172.16.99.10/24 dev enp2s0

Lanzar iperf en modo servidor en cada ordenador

	iperf -s

Comprueba con netstat en qué puerto escucha

	netstat -tlnp
		Active Internet connections (only servers)
	Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
	tcp        0      0 0.0.0.0:5001            0.0.0.0:*               LISTEN      11573/iperf   
	[...]      

Conectarse desde otro pc como cliente

	iperf -c 172.16.99.11
	------------------------------------------------------------
	Client connecting to 172.16.99.11, TCP port 5001
	TCP window size: 85.0 KByte (default)
	------------------------------------------------------------
	[  3] local 172.16.99.10 port 58738 connected with 172.16.99.11 port 5001
	[ ID] Interval       Transfer     Bandwidth
	[  3]  0.0-10.0 sec   112 MBytes  93.8 Mbits/sec


Repetir el procedimiento y capturar los 30 primeros paquetes con tshark

	tshark -i enp2s0 -w /tmp/test4.pcap -c 30
	Running as user "root" and group "root". This could be dangerous.
	Capturing on 'enp2s0'
	30 


Encontrar los 3 paquetes del handshake de tcp

	tshark -r /tmp/test4.pcap -x -Y "frame.number==8"
Running as user "root" and group "root". This could be dangerous.
0000  14 da e9 b6 e8 ea 14 da e9 99 a1 bc 08 00 45 00   ..............E.
0010  00 3c a8 fb 40 00 40 06 73 8a ac 10 63 0b ac 10   .<..@.@.s...c...
0020  63 0a b2 dc 13 89 42 e5 da 76 00 00 00 00 a0 02   c.....B..v......
0030  72 10 60 9b 00 00 02 04 05 b4 04 02 08 0a 00 cf   r.`.............
0040  72 8d 00 00 00 00 01 03 03 07                     r.........

	tshark -r /tmp/test4.pcap -x -Y "frame.number==9"
Running as user "root" and group "root". This could be dangerous.
0000  14 da e9 99 a1 bc 14 da e9 b6 e8 ea 08 00 45 00   ..............E.
0010  00 3c 00 00 40 00 40 06 1c 86 ac 10 63 0a ac 10   .<..@.@.....c...
0020  63 0b 13 89 b2 dc 75 50 b6 a8 42 e5 da 77 a0 12   c.....uP..B..w..
0030  71 20 84 db 00 00 02 04 05 b4 04 02 08 0a 00 74   q .............t
0040  b0 31 00 cf 72 8d 01 03 03 07                     .1..r.....

	tshark -r /tmp/test4.pcap -x -Y "frame.number==10"
Running as user "root" and group "root". This could be dangerous.
0000  14 da e9 b6 e8 ea 14 da e9 99 a1 bc 08 00 45 00   ..............E.
0010  00 34 a8 fc 40 00 40 06 73 91 ac 10 63 0b ac 10   .4..@.@.s...c...
0020  63 0a b2 dc 13 89 42 e5 da 77 75 50 b6 a9 80 10   c.....B..wuP....
0030  00 e5 23 e3 00 00 01 01 08 0a 00 cf 72 8d 00 74   ..#.........r..t
0040  b0 31                                             .1



Abrir dos servidores en dos puertos distintos

	iperf -s -i 3 -p 5004
	iperf -s -i 4 -p 5005

Observar como quedan esos puertos abiertos con netstat
	
	netstat -utlnp
	[...]
	tcp        0      0 0.0.0.0:5004            0.0.0.0:*               LISTEN      2100/iperf    

Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

	iperf -t 60 -c 172.16.99.11

Mientras tanto con netstat mirar conexiones abiertas

	netstat -utlnp
